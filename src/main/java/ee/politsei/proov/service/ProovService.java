package ee.politsei.proov.service;

import ee.politsei.proov.dto.TwoValueSumDto;
import ee.politsei.proov.exceptions.ParameterOutOfRangeException;
import ee.politsei.proov.mapper.TwoValueSumMapper;
import ee.politsei.proov.repository.ProovRepository;
import ee.politsei.proov.repository.TwoValueSum;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ProovService {

    private final ProovRepository proovRepository;
    private final TwoValueSumMapper twoValueSumMapper;
    public final static int LOWER_BOUND = 0;
    public final static int UPPER_BOUND = 100;

    public List<TwoValueSumDto> getSortedEntries(Integer match, boolean ascending) {
        List<TwoValueSum> list = getListFromRepository(match);
        return twoValueSumMapper.toDtoList(sortListBySum(list, ascending));
    }

    private List<TwoValueSum> getListFromRepository(Integer match) {
        if (match == null) {
            return proovRepository.getAll();
        } else {
            return proovRepository.searchByValuesOrSum(match);
        }
    }

    private List<TwoValueSum> sortListBySum(List<TwoValueSum> list, boolean ascending) {
        Comparator<TwoValueSum> comparator = Comparator.comparingInt(TwoValueSum::getSum);
        return list.stream().sorted(ascending ? comparator : comparator.reversed()).toList();
    }


    public TwoValueSumDto save(int val1, int val2) {
        validateInputs(val1, val2);
        return twoValueSumMapper.toDto(proovRepository.save(new TwoValueSum(val1, val2)));
    }

    private void validateInputs(int val1, int val2) {
        if (inputNotValid(val1) || inputNotValid(val2)) {
            throw new ParameterOutOfRangeException();
        }
    }

    private boolean inputNotValid(int val) {
        return val < LOWER_BOUND || val > UPPER_BOUND;
    }


}
