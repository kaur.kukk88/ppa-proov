package ee.politsei.proov.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import static ee.politsei.proov.utils.CustomMessages.MISSING_PARAMETER;

@ControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(ParameterOutOfRangeException.class)
    public ResponseEntity<String> handleParameterOutOfBounds(ParameterOutOfRangeException ex) {
        return new ResponseEntity<>(ex.getMessage(), ex.getHttpStatus());
    }

    @ExceptionHandler(MissingServletRequestParameterException.class)
    public ResponseEntity<String> handleMissingParams(MissingServletRequestParameterException ex) {
        String paramName = ex.getParameterName();
        return new ResponseEntity<>(MISSING_PARAMETER + paramName, HttpStatus.BAD_REQUEST);
    }
}
