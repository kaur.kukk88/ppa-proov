package ee.politsei.proov.mapper;

import ee.politsei.proov.dto.TwoValueSumDto;
import ee.politsei.proov.repository.TwoValueSum;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface TwoValueSumMapper {

    TwoValueSumDto toDto(TwoValueSum twoValueSum);

    List<TwoValueSumDto> toDtoList(List<TwoValueSum> list);
}
