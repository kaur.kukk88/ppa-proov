package ee.politsei.proov.repository;

import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Component
public class ProovRepository {
    final List<TwoValueSum> repository = Collections.synchronizedList(new ArrayList<>());

    public TwoValueSum save(TwoValueSum entry) {
        repository.add(entry);
        return entry;
    }

    public List<TwoValueSum> searchByValuesOrSum(int match) {
        synchronized (repository) {
            return repository.stream().filter(entry -> entry.isEqualToAnyField(match)).toList();
        }
    }

    public List<TwoValueSum> getAll() {
        synchronized (repository) {
            return new ArrayList<>(repository);
        }
    }

    public void clear() {
        repository.clear();
    }

}
