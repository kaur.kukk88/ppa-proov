package ee.politsei.proov.controller;

import ee.politsei.proov.dto.TwoValueSumDto;
import ee.politsei.proov.service.ProovService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static ee.politsei.proov.utils.CustomMessages.NO_MATCH_IN_DATABASE;

@RestController
@RequiredArgsConstructor
public class ProovController {

    private final ProovService proovService;

    @GetMapping("/twoValueSum")
    public TwoValueSumDto twoValueSum(
            @RequestParam("val1") Integer val1,
            @RequestParam("val2") Integer val2
    ) {
        return proovService.save(val1, val2);
    }

    @GetMapping("/getSorted")
    public ResponseEntity<?> getSorted(
            @RequestParam(value = "match", required = false) Integer match,
            @RequestParam("ascending") boolean ascending
    ) {
        List<TwoValueSumDto> list = proovService.getSortedEntries(match, ascending);
        if (list.isEmpty()) {
            return new ResponseEntity<>(NO_MATCH_IN_DATABASE, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(list, HttpStatus.OK);
    }

}
