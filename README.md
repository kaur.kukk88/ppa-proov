# PPA-proov
## Spring Boot 3.x Application with Arithmetic Operations

### Overview
This Spring Boot 3.x application is developed using Java 17 and Maven. 
It features an object model that represents the sum of two integers and provides 
two RESTful services: one for calculating the sum and another for querying past sums 
based on specific criteria.

### Endpoints
#### Arithmetic Service
- **Endpoint**: `/twoValueSum`
- **Method**: GET
- **Query Parameters**:
  - `val1`: Integer between 0 and 100 (inclusive).
  - `val2`: Integer between 0 and 100 (inclusive).
- **Full URL Example**: `http://localhost:8080/twoValueSum?val1=10&val2=20`

#### Query Service
- **Endpoint**: `/getSorted`
- **Method**: GET
- **Query Parameters**:
  - `match`: Optional integer between 0 and 100 (inclusive).
  - `ascending`: Boolean value to sort the results in ascending or descending order.
- **Full URL Example**: `http://localhost:8080/getSorted?match=30&ascending=true`


### Architectural Overview

The application follows a layered architectural pattern for separation of concerns:

#### Controller Layer
- **Responsibility**: Manages HTTP requests and responses.
- **Technologies**: Spring's `@RestController`.

#### Service Layer
- **Responsibility**: Contains the business logic for arithmetic operations and querying.
- **Technologies**: Plain Java classes annotated with Spring's `@Component`.

#### Repository Layer
- **Responsibility**: Manages a `LinkedList` that simulates a database for storing TwoValueSum objects.
- **Technologies**: Plain Java classes annotated with Spring's `@Component`.

#### Data Transfer Object (DTO)
- **Responsibility**: Used for encapsulating the data that is transferred between the server and client.
- **Technologies**: Plain Java classes annotated with Lombok's `@Data`.

#### Global Exception Handling
- **Responsibility**: Centralized handling of exceptions.
- **Technologies**: Spring's `@ControllerAdvice` and `@ExceptionHandler`.

### Thread Safety
- **Repository Layer**: The `LinkedList` used for simulating the database is made thread-safe using `Collections.synchronizedList()`. This ensures that multiple threads can safely interact with the repository without causing race conditions.


### Features
#### Arithmetic Service
- **Method**: GET
- **Input**: Takes two integer values (both are mandatory) ranging between 0 and 100 (inclusive).
- **Output**: Returns an object containing the two integers and their sum.

#### Query Service
- **Method**: GET
- **Input**:
    - An optional integer (between 0 and 100 inclusive) to filter the entries.
    - A mandatory attribute to sort the results in ascending or descending order.

- **Output**:
    - If an integer is provided, it filters the sums where either of the integers or the sum matches the given integer.
    - Results are sorted based on the sum, either in ascending or descending order, depending on the sort attribute provided.

### Technologies and Libraries
- **MapStruct**: To generate code for DTO mapping classes.
- **Lombok**: Utilized to minimize boilerplate code.

### Testing
- All endpoint functionalities are covered by unit tests to ensure reliability and correctness. The tests have been written using JUnit and MockMvc for endpoint testing.

### Usage
- This application serves as an arithmetic calculator as well as a search engine to filter and sort past arithmetic operations.
