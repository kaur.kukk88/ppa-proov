package ee.politsei.proov.dto;


import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class TwoValueSumDto {
    private int val1;
    private int val2;
    private int sum;
}
