package ee.politsei.proov.utils;

public class CustomMessages {

    public static final String NO_MATCH_IN_DATABASE = "No match in search, or database is empty";
    public static final String MISSING_PARAMETER = "Missing parameter: ";
}
