package ee.politsei.proov.exceptions;

import lombok.Getter;
import org.springframework.http.HttpStatus;

import static ee.politsei.proov.service.ProovService.LOWER_BOUND;
import static ee.politsei.proov.service.ProovService.UPPER_BOUND;
import static java.lang.String.format;

@Getter
public class ParameterOutOfRangeException extends RuntimeException {

    public static final String MESSAGE = format("Value needs to be in range of %d and %d", LOWER_BOUND, UPPER_BOUND);
    private final HttpStatus httpStatus = HttpStatus.BAD_REQUEST;

    public ParameterOutOfRangeException() {
        super(MESSAGE);
    }
}