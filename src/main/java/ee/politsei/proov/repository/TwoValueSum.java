package ee.politsei.proov.repository;

import lombok.Getter;

@Getter
public class TwoValueSum {
    int val1;
    int val2;
    int sum;

    public TwoValueSum(int val1, int val2) {
        this.val1 = val1;
        this.val2 = val2;
        sum = val1 + val2;
    }

    public boolean isEqualToAnyField(int val) {
        return val1 == val || val2 == val || sum == val;
    }
}
