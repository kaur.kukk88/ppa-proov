package ee.politsei.proov;

import ee.politsei.proov.exceptions.ParameterOutOfRangeException;
import ee.politsei.proov.repository.ProovRepository;
import ee.politsei.proov.utils.CustomMessages;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
class PpaProovApplicationTests {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ProovRepository proovRepository;


    @BeforeEach
    public void setup() {
        proovRepository.clear();
    }

    @Nested
    @SpringBootTest
    class TwoValueSumTest {

        @Test
        @DisplayName("Object data is correct.")
        public void correctObjectData() throws Exception {
            mockMvc.perform(get("/twoValueSum?val1=1&val2=2"))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.val1").value(1))
                    .andExpect(jsonPath("$.val2").value(2))
                    .andExpect(jsonPath("$.sum").value(3));
        }

        @Test
        @DisplayName("Val1 is too small")
        void val1ToSmall() throws Exception {
            mockMvc.perform(get("/twoValueSum?val1=-1&val2=0"))
                    .andExpect(status().isBadRequest())
                    .andExpect(content().string(ParameterOutOfRangeException.MESSAGE));
        }

        @Test
        @DisplayName("Val2 is too small")
        void val2ToSmall() throws Exception {
            mockMvc.perform(get("/twoValueSum?val1=1&val2=-1"))
                    .andExpect(status().isBadRequest())
                    .andExpect(content().string(ParameterOutOfRangeException.MESSAGE));
        }

        @Test
        @DisplayName("Val1 is too large")
        void val1ToLarge() throws Exception {
            mockMvc.perform(get("/twoValueSum?val1=101&val2=0"))
                    .andExpect(status().isBadRequest())
                    .andExpect(content().string(ParameterOutOfRangeException.MESSAGE));
        }

        @Test
        @DisplayName("Val2 is too large")
        void val2ToLarge() throws Exception {
            mockMvc.perform(get("/twoValueSum?val1=1&val2=101"))
                    .andExpect(status().isBadRequest())
                    .andExpect(content().string(ParameterOutOfRangeException.MESSAGE));
        }


        @Test
        @DisplayName("Parameter Val2 is missing")
        public void missingParameter() throws Exception {
            mockMvc.perform(get("/twoValueSum?val1=1"))
                    .andExpect(status().isBadRequest())
                    .andExpect(content().string(CustomMessages.MISSING_PARAMETER + "val2"));
        }

        @Test
        @DisplayName("Parameter Val1 is missing")
        public void missingParameter2() throws Exception {
            mockMvc.perform(get("/twoValueSum?val2=1"))
                    .andExpect(status().isBadRequest())
                    .andExpect(content().string(CustomMessages.MISSING_PARAMETER + "val1"));
        }
    }

    @Nested
    @SpringBootTest
    class GetSortedTest {

        @Test
        @DisplayName("Ascending is required")
        void ascendingIsRequired() throws Exception {
            mockMvc.perform(get("/getSorted"))
                    .andExpect(status().isBadRequest());
        }

        @Test
        @DisplayName("List is sorted ascending, match not defined")
        void sortAscending() throws Exception {
            int entries = 5;
            for (int i = 0; i < entries; i++) {
                mockMvc.perform(get("/twoValueSum?val1=0&val2=" + i));
            }
            ResultActions actions = mockMvc.perform(get("/getSorted?ascending=true"));
            for (int i = 0; i < entries; i++) {
                actions.andExpect(jsonPath("$[%d].sum".formatted(i)).value(i));
            }
        }

        @Test
        @DisplayName("List is sorted ascending, match defined")
        void sortAscendingWithMatch() throws Exception {
            mockMvc.perform(get("/twoValueSum?val1=5&val2=1"));
            mockMvc.perform(get("/twoValueSum?val1=1&val2=4"));
            mockMvc.perform(get("/twoValueSum?val1=2&val2=5"));
            mockMvc.perform(get("/getSorted?ascending=true&match=5"))
                    .andExpect(jsonPath("$[0].sum").value(5))
                    .andExpect(jsonPath("$[1].sum").value(6))
                    .andExpect(jsonPath("$[2].sum").value(7));
        }

        @Test
        @DisplayName("List is sorted descending, match not defined")
        void sortDescending() throws Exception {
            int entries = 5;
            for (int i = 0; i < entries; i++) {
                mockMvc.perform(get("/twoValueSum?val1=0&val2=" + i));
            }
            ResultActions actions = mockMvc.perform(get("/getSorted?ascending=false"));
            for (int i = 0; i < entries; i++) {
                actions.andExpect(jsonPath("$[%d].sum".formatted(i)).value(entries - 1 - i));
            }
        }

        @Test
        @DisplayName("List is sorted descending, match defined")
        void sortDescendingWithMatch() throws Exception {
            mockMvc.perform(get("/twoValueSum?val1=5&val2=1"));
            mockMvc.perform(get("/twoValueSum?val1=1&val2=4"));
            mockMvc.perform(get("/twoValueSum?val1=2&val2=5"));
            mockMvc.perform(get("/getSorted?ascending=false&match=5"))
                    .andExpect(jsonPath("$[0].sum").value(7))
                    .andExpect(jsonPath("$[1].sum").value(6))
                    .andExpect(jsonPath("$[2].sum").value(5));
        }

        @Test
        @DisplayName("Returns all if match not defined")
        void returnsAllIfMatchNotDefined() throws Exception {
            int entries = 5;
            for (int i = 0; i < entries; i++) {
                mockMvc.perform(get("/twoValueSum?val1=0&val2=" + i));
            }
            mockMvc.perform(get("/getSorted?ascending=true"))
                    .andExpect(jsonPath("$.length()").value(entries));
        }


        @Test
        @DisplayName("Match val1 only")
        void matchReturnsVal1() throws Exception {

            mockMvc.perform(get("/twoValueSum?val1=5&val2=1"));
            mockMvc.perform(get("/twoValueSum?val1=2&val2=8"));
            mockMvc.perform(get("/twoValueSum?val1=5&val2=8"));
            mockMvc.perform(get("/getSorted?ascending=true&match=5"))
                    .andExpect(jsonPath("$.length()").value(2));
        }

        @Test
        @DisplayName("Match val2 only")
        void matchReturnsVal2() throws Exception {

            mockMvc.perform(get("/twoValueSum?val1=5&val2=1"));
            mockMvc.perform(get("/twoValueSum?val1=2&val2=8"));
            mockMvc.perform(get("/twoValueSum?val1=2&val2=1"));
            mockMvc.perform(get("/getSorted?ascending=true&match=1"))
                    .andExpect(jsonPath("$.length()").value(2));
        }

        @Test
        @DisplayName("Match val1 or val2 ")
        void matchReturnsVal1Val2() throws Exception {

            mockMvc.perform(get("/twoValueSum?val1=5&val2=1"));
            mockMvc.perform(get("/twoValueSum?val1=1&val2=8"));
            mockMvc.perform(get("/twoValueSum?val1=2&val2=1"));
            mockMvc.perform(get("/getSorted?ascending=true&match=1"))
                    .andExpect(jsonPath("$.length()").value(3));
        }

        @Test
        @DisplayName("Match sum only")
        void matchSumOnly() throws Exception {

            mockMvc.perform(get("/twoValueSum?val1=5&val2=1"));
            mockMvc.perform(get("/twoValueSum?val1=1&val2=8"));
            mockMvc.perform(get("/twoValueSum?val1=2&val2=1"));
            mockMvc.perform(get("/getSorted?ascending=true&match=6"))
                    .andExpect(jsonPath("$.length()").value(1));
        }

        @Test
        @DisplayName("Match val1 or val2 or sum")
        void matchAny() throws Exception {

            mockMvc.perform(get("/twoValueSum?val1=5&val2=1"));
            mockMvc.perform(get("/twoValueSum?val1=1&val2=4"));
            mockMvc.perform(get("/twoValueSum?val1=2&val2=5"));
            mockMvc.perform(get("/getSorted?ascending=true&match=5"))
                    .andExpect(jsonPath("$.length()").value(3));
        }
    }

}
